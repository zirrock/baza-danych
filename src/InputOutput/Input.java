package InputOutput;

import Structures.Baza;
import java.io.*;

public class Input {
    public static Baza readBazaFromFile(String directory, String fileName)
    {
        Baza fqt;
        try{
            FileInputStream fileIn = new FileInputStream(directory + fileName + ".fqt");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            fqt = (Baza) in.readObject();
            in.close();
            fileIn.close();
            if(fqt == null) {
                System.out.println("here");
                return new Baza();
            }
        } catch (IOException e)
        {
            return new Baza();
        } catch (ClassNotFoundException e)
        {
            return new Baza();
        }
        return fqt;
    }

}
