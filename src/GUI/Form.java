package GUI;

import InputOutput.Input;
import InputOutput.Output;
import Structures.Baza;
import Structures.Faktura;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;


public class Form {

    public Form(Baza base) {
        this.baza = base;
        Form t = this;

        list1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listModel = new DefaultListModel();
        for(Faktura i: baza.getFaktury())
            listModel.addElement(i);
        list1.setModel(listModel);

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Faktura fqt = new Faktura();
                listModel.addElement(fqt);
                baza.dodajFakturę(fqt);
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        NewFakturaDialog dialog = new NewFakturaDialog(baza, fqt, list1);
                        dialog.pack();
                        dialog.setVisible(true);
                    }
                });
            }
        });
        list1.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
            }
        });
        editButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Faktura fqt = list1.getSelectedValue();
                if (fqt == null) return;
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        NewFakturaDialog dialog = new NewFakturaDialog(baza, fqt, list1);
                        dialog.pack();
                        dialog.setVisible(true);
                    }
                });
            }
        });
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int selectedIndex = list1.getSelectedIndex();
                if(selectedIndex != -1)
                {
                    listModel.remove(selectedIndex);
                    baza.usuńFakturę(list1.getSelectedValue());
                }

            }
        });
    }

    public static void main(String[] args) {
        String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String dir = rootPath + "/faktury/";
        Baza base = Input.readBazaFromFile(dir, "faktury");
        JFrame frame = new JFrame("Form");
        frame.setContentPane(new Form(base).panel);
        frame.addWindowListener(new WindowAdapter() {
            //I skipped unused callbacks for readability

            @Override
            public void windowClosing(WindowEvent e) {
                Output.createFileFromBaza(dir, base, "faktury");
                frame.setVisible(false);
                frame.dispose();
            }
        });
        frame.pack();
        frame.setVisible(true);
    }

    private Baza baza;

    private DefaultListModel listModel;
    private JPanel panel;
    private JButton button1;
    private JList <Faktura> list1;
    private JButton deleteButton;
    private JButton editButton;
}
