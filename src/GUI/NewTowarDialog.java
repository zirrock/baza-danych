package GUI;

import Structures.Towar;

import javax.swing.*;
import java.awt.event.*;

public class NewTowarDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField nazwaTextField;
    private JTextField miaraTextField;
    private JTextField iloscTextField;
    private JTextField cenaTextField;
    private JTextField podatekTextField;
    private JTextArea podstawaPrawnaTextArea;

    private Towar towar;
    private JList <Towar> towarList;

    public NewTowarDialog(Towar towar, JList towarList) {
        this.towar = towar;
        this.towarList = towarList;

        aktualizujOkno(towar);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        pack();

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        // add your code here
        aktualizujTowar(towar);
        towarList.repaint();
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    private void aktualizujTowar(Towar towar)
    {
        towar.setNazwa(nazwaTextField.getText());
        towar.setMiara(miaraTextField.getText());
        if(iloscTextField.getText() == "") towar.setIlosc(0f);
        towar.setIlosc(Float.parseFloat(iloscTextField.getText()));
        towar.setCenaJednostkowa(Integer.parseInt(cenaTextField.getText()));
        towar.setProcentPodatku(Integer.parseInt(podatekTextField.getText()));
        towar.setPodstawaPrawna(podstawaPrawnaTextArea.getText());
    }

    private void aktualizujOkno(Towar towar)
    {
        nazwaTextField.setText(towar.getNazwa());
        miaraTextField.setText(towar.getMiara());
        iloscTextField.setText(Float.toString(towar.getIlosc()));
        cenaTextField.setText(Integer.toString(towar.getCenaJednostkowa()));
        podatekTextField.setText(Integer.toString(towar.getProcentPodatku()));
        podstawaPrawnaTextArea.setText(towar.getPodstawaPrawna());
    }
}
