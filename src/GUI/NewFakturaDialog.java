package GUI;

import Structures.Baza;
import Structures.Faktura;
import Structures.Towar;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.print.*;

public class NewFakturaDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField sprzedawcaNazwaTextField;
    private JTextField sprzedawcaAdresTextField;
    private JTextField sprzedawcaNIPTextField;
    private JTextField nabywcaNazwaTextField;
    private JTextField nabywcaAdresTextField;
    private JTextField nabywcaNIPTextField;
    private JTextField dataMiejscowoscTextField;
    private JTextField numberFakturyTextField;
    private JTextField dataDzienTextField;
    private JTextField dataRokTextField;
    private JTextField dataMiesiacTextField;
    private JList <Towar> towarList;
    private JButton dodajButton;
    private JButton edytujButton;
    private JButton usuńButton;
    private JTextField zaplataSposobTextField;
    private JTextField zaplataBankTextField;
    private JTextField zaplataKontoTextField;
    private JTextField zaplataTerminTextField;
    private JTextArea adnotacjeTextArea;
    private JButton drukujButton;

    private DefaultListModel listModel;

    Baza baza;
    Faktura faktura;
    JList <Faktura> lista;

    public NewFakturaDialog(Baza baza, Faktura fqt, JList lista) {
        this.baza = baza;
        this.faktura = fqt;
        this.lista = lista;
        Towar t = new Towar();

        listModel = new DefaultListModel();
        towarList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        aktualizujOkno(faktura);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        dodajButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Towar towar = new Towar();
                faktura.addTowar(towar);
                listModel.addElement(towar);
                towarList.setModel(listModel);
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        NewTowarDialog dialog = new NewTowarDialog(towar, towarList);
                        dialog.pack();
                        dialog.setVisible(true);
                    }
                });
            }
        });
        edytujButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Towar t = towarList.getSelectedValue();
                if (t == null) return;
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        NewTowarDialog dialog = new NewTowarDialog(t, towarList);
                        dialog.pack();
                        dialog.setVisible(true);
                    }
                });
            }
        });
        usuńButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedIndex = towarList.getSelectedIndex();
                if(selectedIndex != -1)
                {
                    listModel.remove(selectedIndex);
                }
            }
        });
        drukujButton.addActionListener(new FakturaPrinter());
    }

    private void onOK() {
        aktualizujFakture(faktura);
        lista.repaint();
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    private void aktualizujOkno(Faktura faktura)
    {
        //dane nagłowkowe
        numberFakturyTextField.setText(faktura.getNumerFaktury());
        dataRokTextField.setText(faktura.getDataWystawieniaRok());
        dataMiesiacTextField.setText(faktura.getDataWystawieniaMiesiac());
        dataDzienTextField.setText(faktura.getGetDataWystawieniaDzien());
        dataMiejscowoscTextField.setText(faktura.getMiejsceWystawienia());

        //dane sprzedawcy
        sprzedawcaNazwaTextField.setText(faktura.getNazwaSprzedawcy());
        sprzedawcaAdresTextField.setText(faktura.getAdresSprzedawcy());
        sprzedawcaNIPTextField.setText(faktura.getNIPSprzedawcy());

        //dane nabywcy
        nabywcaNazwaTextField.setText(faktura.getNazwaNabywcy());
        nabywcaAdresTextField.setText(faktura.getAdresNabywcy());
        nabywcaNIPTextField.setText(faktura.getNIPNabywcy());

        //towary
        DefaultListModel <Towar> listModel = new DefaultListModel<>();
        for(Towar i : faktura.getTowary())
        {
            listModel.addElement(i);
        }
        towarList.setModel(listModel);

        // dane zapłaty
        zaplataSposobTextField.setText(faktura.getSposobZaplaty());
        zaplataTerminTextField.setText(faktura.getTerminZaplaty());
        zaplataBankTextField.setText(faktura.getBankZaplaty());
        zaplataKontoTextField.setText(faktura.getNumerKontaZaplaty());
        adnotacjeTextArea.setText(faktura.getAdnotacje());
    }

    private void aktualizujFakture(Faktura faktura)
    {
        // dane nagłówkowe
        faktura.setNumerFaktury(numberFakturyTextField.getText());
        faktura.setDataWystawieniaRok(dataRokTextField.getText());
        faktura.setDataWystawieniaMiesiac(dataMiesiacTextField.getText());
        faktura.setGetDataWystawieniaDzien(dataDzienTextField.getText());
        faktura.setMiejsceWystawienia(dataMiejscowoscTextField.getText());

        // dane sprzedawcy
        faktura.setNazwaSprzedawcy(sprzedawcaNazwaTextField.getText());
        faktura.setAdresSprzedawcy(sprzedawcaAdresTextField.getText());
        faktura.setNIPSprzedawcy(sprzedawcaNIPTextField.getText());

        //dane nabywcy
        faktura.setNazwaNabywcy(nabywcaNazwaTextField.getText());
        faktura.setAdresNabywcy(nabywcaAdresTextField.getText());
        faktura.setNIPNabywcy(nabywcaNIPTextField.getText());

        // towary
        ListModel<Towar> listModel = towarList.getModel();
        ArrayList<Towar> towary = new ArrayList<>();
        for(int i = 0; i < listModel.getSize(); i++)
        {
            towary.add(listModel.getElementAt(i));
        }
        faktura.setTowary(towary);

        // dane zapłaty
        faktura.setSposobZaplaty(zaplataSposobTextField.getText());
        faktura.setTerminZaplaty(zaplataTerminTextField.getText());
        faktura.setBankZaplaty(zaplataBankTextField.getText());
        faktura.setNumerKontaZaplaty(zaplataKontoTextField.getText());

        faktura.setAdnotacje(adnotacjeTextArea.getText());
    }

    public class FakturaPrinter implements Printable, ActionListener {
        public int print(Graphics g, PageFormat pf, int page)
                throws PrinterException {
            if (page > 0) {
                return NO_SUCH_PAGE;
            }

            Graphics2D g2d = (Graphics2D)g;
            g2d.translate(pf.getImageableX(), pf.getImageableY());

            // Print the entire visible contents of a
            // java.awt.Frame.
            printAll(g);

            return PAGE_EXISTS;
        }

        public void actionPerformed(ActionEvent e) {
            PrinterJob job = PrinterJob.getPrinterJob();
            job.setPrintable(this);
            boolean ok = job.printDialog();
            if (ok) {
                try {
                    job.print();
                } catch (PrinterException ex) {
                    /* The job did not successfully complete */
                }
            }
        }

    }
}
