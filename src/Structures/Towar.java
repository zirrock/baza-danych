package Structures;

import java.io.Serializable;

public class Towar implements Serializable{

    private String nazwa;
    private String miara;
    private Float ilosc;
    private String podstawaPrawna;
    private int cenaJednostkowa;
    private int procentPodatku;
    private int cenaSumaryczna;

    public Towar()
    {
        nazwa = "";
        miara = "";
        ilosc = 0f;
        podstawaPrawna = "";
        cenaJednostkowa = 0;
        procentPodatku = 0;
        cenaSumaryczna = 0;

    }

    @Override
    public String toString()
    {
        if(nazwa.equals("")) return "<pusty>";
        return nazwa;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getMiara() {
        return miara;
    }

    public void setMiara(String miara) {
        this.miara = miara;
    }

    public Float getIlosc() {
        return ilosc;
    }

    public void setIlosc(Float ilosc) {
        this.ilosc = ilosc;
    }

    public String getPodstawaPrawna() {
        return podstawaPrawna;
    }

    public void setPodstawaPrawna(String podstawaPrawna) {
        this.podstawaPrawna = podstawaPrawna;
    }

    public int getCenaJednostkowa() {
        return cenaJednostkowa;
    }

    public void setCenaJednostkowa(int cenaJednostkowa) {
        this.cenaJednostkowa = cenaJednostkowa;
    }

    public int getProcentPodatku() {
        return procentPodatku;
    }

    public void setProcentPodatku(int procentPodatku) {
        this.procentPodatku = procentPodatku;
    }

    public int getCenaSumaryczna() {
        return cenaSumaryczna;
    }

    public void setCenaSumaryczna(int cenaSumaryczna) {
        this.cenaSumaryczna = cenaSumaryczna;
    }
}
