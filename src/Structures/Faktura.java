package Structures;

import java.util.ArrayList;
import java.io.Serializable;

public class Faktura implements Serializable{

    public Faktura()
    {
        this.numerFaktury = "";
        this.towary = new ArrayList<>();
    }

    public void addTowar(Towar t)
    {
        towary.add(t);
    }

    @Override
    public String toString()
    {
        if ("".equals(numerFaktury)) return "<pusta>";
        return numerFaktury;
    }

    private String numerFaktury;
    private String dataWystawieniaRok;
    private String dataWystawieniaMiesiac;
    private String getDataWystawieniaDzien;
    private String miejsceWystawienia;

    //dabe sprzedawcy
    private String nazwaSprzedawcy;
    private String adresSprzedawcy;
    private String NIPSprzedawcy;

    // dane nabywcy
    private String nazwaNabywcy;
    private String adresNabywcy;
    private String NIPNabywcy;
    
    private ArrayList<Towar> towary;

    // dane zapłaty
    private String sposobZaplaty;
    private String terminZaplaty;
    private String bankZaplaty;
    private String numerKontaZaplaty;

    // kwota do zapłaty
    private int sumaCen;
    private int procentPodatku;
    private int kwotaPodatku;
    private int kwotaZapłąty;
    private String złoteZaplata;
    private String groszeZaplata;
    private String słownieZaplata;

    private String adnotacje;

    public String getNumerFaktury() {
        return numerFaktury;
    }

    public void setNumerFaktury(String numerFaktury) {
        this.numerFaktury = numerFaktury;
    }

    public String getDataWystawieniaRok() {
        return dataWystawieniaRok;
    }

    public void setDataWystawieniaRok(String dataWystawieniaRok) {
        this.dataWystawieniaRok = dataWystawieniaRok;
    }

    public String getDataWystawieniaMiesiac() {
        return dataWystawieniaMiesiac;
    }

    public void setDataWystawieniaMiesiac(String dataWystawieniaMiesiac) {
        this.dataWystawieniaMiesiac = dataWystawieniaMiesiac;
    }

    public String getGetDataWystawieniaDzien() {
        return getDataWystawieniaDzien;
    }

    public void setGetDataWystawieniaDzien(String getDataWystawieniaDzien) {
        this.getDataWystawieniaDzien = getDataWystawieniaDzien;
    }

    public String getMiejsceWystawienia() {
        return miejsceWystawienia;
    }

    public void setMiejsceWystawienia(String miejsceWystawienia) {
        this.miejsceWystawienia = miejsceWystawienia;
    }

    public String getNazwaSprzedawcy() {
        return nazwaSprzedawcy;
    }

    public void setNazwaSprzedawcy(String nazwaSprzedawcy) {
        this.nazwaSprzedawcy = nazwaSprzedawcy;
    }

    public String getAdresSprzedawcy() {
        return adresSprzedawcy;
    }

    public void setAdresSprzedawcy(String adresSprzedawcy) {
        this.adresSprzedawcy = adresSprzedawcy;
    }

    public String getNIPSprzedawcy() {
        return NIPSprzedawcy;
    }

    public void setNIPSprzedawcy(String NIP) {
        this.NIPSprzedawcy = NIP;
    }

    public String getNazwaNabywcy() {
        return nazwaNabywcy;
    }

    public void setNazwaNabywcy(String nazwaNabywcy) {
        this.nazwaNabywcy = nazwaNabywcy;
    }

    public String getAdresNabywcy() {
        return adresNabywcy;
    }

    public void setAdresNabywcy(String adresNabywcy) {
        this.adresNabywcy = adresNabywcy;
    }

    public String getNIPNabywcy() {
        return NIPNabywcy;
    }

    public void setNIPNabywcy(String NIPNabywcy) {
        this.NIPNabywcy = NIPNabywcy;
    }

    public ArrayList<Towar> getTowary() {
        return towary;
    }

    public void setTowary(ArrayList<Towar> towary) {
        this.towary = towary;
    }

    public String getSposobZaplaty() {
        return sposobZaplaty;
    }

    public void setSposobZaplaty(String sposobZaplaty) {
        this.sposobZaplaty = sposobZaplaty;
    }

    public String getTerminZaplaty() {
        return terminZaplaty;
    }

    public void setTerminZaplaty(String terminZaplaty) {
        this.terminZaplaty = terminZaplaty;
    }

    public String getBankZaplaty() {
        return bankZaplaty;
    }

    public void setBankZaplaty(String bankZaplaty) {
        this.bankZaplaty = bankZaplaty;
    }

    public String getNumerKontaZaplaty() {
        return numerKontaZaplaty;
    }

    public void setNumerKontaZaplaty(String numerKontaZaplaty) {
        this.numerKontaZaplaty = numerKontaZaplaty;
    }

    public int getSumaCen() {
        return sumaCen;
    }

    public void setSumaCen(int sumaCen) {
        this.sumaCen = sumaCen;
    }

    public int getProcentPodatku() {
        return procentPodatku;
    }

    public void setProcentPodatku(int procentPodatku) {
        this.procentPodatku = procentPodatku;
    }

    public int getKwotaPodatku() {
        return kwotaPodatku;
    }

    public void setKwotaPodatku(int kwotaPodatku) {
        this.kwotaPodatku = kwotaPodatku;
    }

    public int getKwotaZapłąty() {
        return kwotaZapłąty;
    }

    public void setKwotaZapłąty(int kwotaZapłąty) {
        this.kwotaZapłąty = kwotaZapłąty;
    }

    public String getZłoteZaplata() {
        return złoteZaplata;
    }

    public void setZłoteZaplata(String złoteZaplata) {
        this.złoteZaplata = złoteZaplata;
    }

    public String getGroszeZaplata() {
        return groszeZaplata;
    }

    public void setGroszeZaplata(String groszeZaplata) {
        this.groszeZaplata = groszeZaplata;
    }

    public String getSłownieZaplata() {
        return słownieZaplata;
    }

    public void setSłownieZaplata(String słownieZaplata) {
        this.słownieZaplata = słownieZaplata;
    }

    public String getAdnotacje() {
        return adnotacje;
    }

    public void setAdnotacje(String adnotacje) {
        this.adnotacje = adnotacje;
    }
}
