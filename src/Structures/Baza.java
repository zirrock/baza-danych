package Structures;

import java.io.Serializable;
import java.util.ArrayList;

public class Baza implements Serializable {

    private ArrayList<Faktura> faktury;

    public Baza()
    {
        faktury = new ArrayList<>();
    }

    public ArrayList<Faktura> getFaktury()
    {
        return faktury;
    }

    public void dodajFakturę(Faktura fqt)
    {
        faktury.add(fqt);
    }

    public void usuńFakturę(Faktura fqt) { faktury.remove(fqt); }
}
